# Final-TeVeO


# ENTREGA CONVOCATORIA MAYO

# ENTREGA DE PRÁCTICA

## Datos

* Nombre: Jose Antonio Marquez Garcia
* Titulación: Ingenieria en tecnologias de la telecomunicacion
* Cuenta en laboratorios: jose
* Cuenta URJC:ja.marquez.2021@alumnos.urjc.es
* Video básico (url): https://youtu.be/-kasDDciRXg
* Video parte opcional (url): https://youtu.be/BE0aSFKVL9c ; https://youtu.be/xPlnMDTExhc
* Despliegue (url): Tono507.pythonanywhere.com
* Contraseñas:
* Cuenta Admin Site: jose/123456

## Resumen parte obligatoria
*    Se ha implementado todo lo requerido en la parte obligatoria
## Lista partes opcionales

* Nombre parte: favicon
incluis un favicon
* Nombre parte: permitir terminar sesiones
permite terminar tu sesion (en realidad ponerla en por defecto, como si fuera la primera vez que entras)
* Nombre parte: mejora de los test 
Test unitarios de cada vista y test punto a punto, simulando ser un usuario (abrir y modificar cosas en paginas)
* Nombre parte: agregar mas listas
Agregue parcialmente una lista mas ya que era muy pesada
* Nombre parte: (propia) funcionalidades de admin en la propia pagina
he puesto una opcion que si se introduce en settings una contraceña que aparece como contrante al principio del codigo, puede borrar lista de camara (junto sus respectivos comentarios) y cerrar todas las sesiones activas.
