from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path("", include("Te_Veo_App.urls")),
    path("admin/", admin.site.urls),
]
