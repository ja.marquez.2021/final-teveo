import base64
from django.core.exceptions import ObjectDoesNotExist
from django.core.paginator import Paginator
from django.db.models import Count
from django.http import HttpResponse, JsonResponse, FileResponse
from django.shortcuts import redirect
from django.template import loader
from datetime import datetime, timezone
import requests
import os
import xml.etree.ElementTree as ET
from requests import Session
from .models import ListaCamaras, Camara, Comentario, Session_db
from django.http import HttpResponseRedirect
from django.shortcuts import render
from .forms import NameForm, ComentarioForm

################### CONSTANTES ###################
    # numero de camaras por pagina
CAM_PAG_LENG = 5
    # Contraceña para acceder a funcionalidades de admin en la pagina
ADMIN_PASSWORD = "123456"
    # Tipos de letra y tamaños disponibles
AVAILABLE_TYPES = ['Roboto','Open Sans', 'Lato', 'Times New Roman', 'Cursive', 'Monospace', 'Arial']
AVAILABLE_SIZES = [8, 12, 16, 20, 24, 32]
    # Listados disponibles (tienen que tener el mismo nombre que el xml del listado y
    # estar en la carpeta "Available_cam_list")
AVAILABLES_LISTS = ["listado1","listado2","listado3"]
    # Valores iniciales
INIT_LETTER_SIZE = 16
INIT_LETTER_TYPE = "Arial"
INIT_NAME = 'Anónimo'


################### Funcines ###################
def update_sesiondb(request):
    Session_db.objects.update_or_create(
        defaults={'name': request.session['nombre_usuario'],
                  'letter_type': request.session['letter_type'],
                  'letter_size': request.session['letter_size']},
        id=request.session.session_key
    )
def metadata(request):
    camaras = Camara.objects.count()
    comentarios = Comentario.objects.count()
    visitas = Session_db.objects.count()
    # Esto lo hacemos por si es la primera vez que entra una sesion,
    # el contador de visitas tenga en cuenta la visita de ese mismo sesion.
    if not request.session.session_key:
        visitas +=1
    metadata ={'camaras':camaras,
               'comentarios':comentarios,
               'visitas':visitas,
               'admin_flag':request.session['admin_flag'],
               'session_name':request.session['nombre_usuario']}
    return metadata

def manage_id_sesion(request):
    if not request.session.session_key:
        request.session['nombre_usuario'] = INIT_NAME
        request.session['letter_size'] = INIT_LETTER_SIZE
        request.session['letter_type'] = INIT_LETTER_TYPE
        request.session['admin_flag'] = 0
    else:
        update_sesiondb(request)
################### Paginas ###################
def main_page(request):
    manage_id_sesion(request)
    comentarios = Comentario.objects.all().order_by('-fecha')
    if request.GET.get('id'):
        try:
            sesion_by_id= Session_db.objects.filter(id=request.GET.get('id')).first()
            request.session['nombre_usuario'] = sesion_by_id.name
            request.session['letter_size'] = sesion_by_id.letter_type
            request.session['letter_type'] = sesion_by_id.letter_size
        except:
            pass
    body = "body/Main_body.html"
    context = {'nombre_body': body,
               'data': comentarios,
               'metadata':metadata(request)}
    return render(request, 'pages/page.html', context)

def camara_form(request):
    manage_id_sesion(request)
    # if this is a POST request we need to process the form data
    if request.method == "POST":
        # create a form instance and populate it with data from the request:
        form = NameForm(request.POST)
        # check whether it's valid:
        if form.is_valid():
            return HttpResponseRedirect("/")

    # if a GET (or any other method) we'll create a blank form
    else:
        form = ComentarioForm()
        body = "body/camara_form.html"
        context = {'nombre_body': body,
                   "data": form,
                   'metadata':metadata(request)}

    return render(request, "pages/page.html", context)

def cameras_page(request):
    manage_id_sesion(request)

    # esto es una manera de "agregar" datos a mi modelo, en cuanto buenas practicas de programacion seria mejor
    # definirla en models, pero quiero dejarala como ejemplo de que esto se puede hacer y es util para campos que
    # apenas se utilizan.(este campo solo existe en cameras_con_comentarios, que es como nuestro modelo pero con
    # el nuevo campo
    camaras_con_comentarios = Camara.objects.annotate(num_comentarios=Count('comentario'))
    sorted_cameras = camaras_con_comentarios.order_by('-num_comentarios')
    main_camera = Camara.objects.order_by('?').first()

    paginator = Paginator(sorted_cameras, 5)  # Número de elementos por página
    page_number = request.GET.get('page')
    page_obj = paginator.get_page(page_number)

    body = "body/cameras_page_body.html"
    data = {
        'sorted_cameras': page_obj,
        'main_camera': main_camera,
        'availables_lists': AVAILABLES_LISTS,
        'admin_flag': request.session['admin_flag']
    }
    context = {
        'nombre_body': body,
        'data': data,
        'metadata': metadata(request),
        'page_obj': page_obj  # Pasar el objeto de paginación al contexto
    }

    return render(request, 'pages/page.html', context)


def camera_page(request,id):
    manage_id_sesion(request)
    camara = Camara.objects.get(id=id)
    comentarios_camara = Comentario.objects.filter(camara=camara).order_by('-fecha')
    data ={ 'camara':camara,
            'comentarios':comentarios_camara}

    body = "body/camera_page_body.html"

    context = {'nombre_body': body,
               'data': data,
               'metadata':metadata(request)}
    return render(request, 'pages/page.html', context)

def comentar_camara(request):
    manage_id_sesion(request)
    id = request.GET.get('id')
    camera = Camara.objects.get(id=id)
    if request.method == "POST":
        form = ComentarioForm(request.POST)
        if form.is_valid():
            # Esto hace que no se haga save automaticamente
            instancia = form.save(commit=False)
            # Aqui introduzco los datos comentario que quiero
            # que sean automaticos
            if False:
                instancia.imagen_camara.save(imagen.name,imagen,save=True)
            instancia.camara = camera
            instancia.autor = request.session['nombre_usuario']
            instancia.fecha = request.session['fecha']
            instancia.imagen_camara = download_image(camera.url)
            # Ahora si guardo el comentario
            instancia.save()
            return redirect('/')
    else:
        form = ComentarioForm()
        request.session['fecha'] = str(datetime.now())
        body = "body/camara_form.html"
        comentarios = Comentario.objects.filter(camara=camera).order_by('-fecha')
        data ={'form':form,
               'camera':camera,
               'comentarios_camara':comentarios,
               'fecha':datetime.now()}
        context = {'nombre_body': body,
                   "data": data,
                   'metadata':metadata(request)}

    return render(request, "pages/page.html", context)

def settings(request):
    manage_id_sesion(request)
    is_name_in_use=False
    if request.method == "POST":
        if 'letter_size' in request.POST:
            request.session['letter_size'] = request.POST['letter_size']
        if 'letter_type' in request.POST:
            request.session['letter_type'] = request.POST['letter_type']
        if 'admin_flag' in request.POST and request.POST['admin_flag'] == ADMIN_PASSWORD:
            request.session['admin_flag'] = 1
            print(request.session['admin_flag'])
        if "unable_admin" in request.POST:
            request.session['admin_flag'] = 0
        if ((('new_name' in request.POST) and
                Session_db.objects.filter(name=request.POST['new_name']).exists()) and
                not request.POST['new_name'] == request.session['nombre_usuario']):
            is_name_in_use = True
        elif ('new_name' in request.POST):
            request.session['nombre_usuario'] = request.POST['new_name']
        update_sesiondb(request)
    body = "body/settings_body.html"
    data = {"selected_type":request.session.get("letter_type"),
            "selected_size":request.session.get("letter_size"),
            "available_sizes":AVAILABLE_SIZES,
            "available_types":AVAILABLE_TYPES,
            "name_in_use":is_name_in_use,
            'id':request.session.session_key}
    context = {'nombre_body': body,
               "data": data,
               'metadata': metadata(request)}
    return render(request, "pages/page.html", context)

def help(request):
    manage_id_sesion(request)
    body = "body/help_body.html"
    data = {"sesion_name":request.session['nombre_usuario']}
    context = {'nombre_body': body,
               "data": data,
               'metadata': metadata(request)}
    return render(request, "pages/page.html", context)

def json_camara(request,cam_id):
    manage_id_sesion(request)
    camara_con_comentarios = Camara.objects.annotate(num_comentarios=Count('comentario'))
    camara = camara_con_comentarios.get(id=cam_id)
    camara_dict = {'id': camara.id,
                   'url': camara.url,
                   'info': camara.info,
                   'latitude': camara.latitude,
                   'longitude': camara.longitude,
                   'lista': str(camara.lista),
                   'num_comentarios': camara.num_comentarios}
    response = JsonResponse(camara_dict)
    return response

def dyn_page(request, id):
    manage_id_sesion(request)
    camara = Camara.objects.get(id=id)
    comentarios_camara = Comentario.objects.filter(camara=camara).order_by('-fecha')
    data = {'camara': camara,
            'comentarios': comentarios_camara}

    body = "body/camara_dyn_page_body.html"

    context = {'nombre_body': body,
               'data': data,
               'metadata': metadata(request)}
    return render(request, 'pages/page.html', context)

def close_sesion(request):
    request.session['nombre_usuario'] = INIT_NAME
    request.session['letter_size'] = INIT_LETTER_SIZE
    request.session['letter_type'] = INIT_LETTER_TYPE
    request.session['admin_flag'] = 0
    return redirect('/')
def close_sesions(request):
    # Obtener todas las sesiones activas
    sesiones_activas = Session.objects.filter(expire_date__gte=timezone.now())

    for sesion in sesiones_activas:
        sesion['nombre_usuario'] = INIT_NAME
        sesion['letter_size'] = INIT_LETTER_SIZE
        sesion['letter_type'] = INIT_LETTER_TYPE
        sesion['admin_flag'] = 0
    return redirect('/')

################### A partir de aqui son cosas de la pagina dinamica ###################
def imagen_dyn(request, id):
    camara = Camara.objects.get(id=id)
    url = camara.url
    headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:100.0) Gecko/20100101 Firefox/100.0'
    }
    try:
        image = requests.get(url, headers=headers)
        image_b64 = base64.b64encode(image.content).decode('utf-8')
        data = '<img class="camera_image" src="data:image/jpeg;base64,##contenido##">'
        data = data.replace('##contenido##', image_b64)
        response = HttpResponse(data, content_type="image/jpeg")

    except ObjectDoesNotExist:
        response = HttpResponse('<p>Image not found</p>')

    return response

def comentarios_dyn(request, id):
    camara = Camara.objects.get(id=id)
    comentarios_cam = Comentario.objects.filter(camara=camara)
    comentarios_cam = comentarios_cam.order_by('-fecha')
    html_content = "<br>"
    for comentario in comentarios_cam:
        html_content += "<ul>\
            <li>\
            <h2><strong>COMENTARIO:</strong></h2>"+\
            "<p><Strong>Fecha:</strong>"+ comentario.fecha.strftime("%Y-%m-%d-%H:%M") +"</p>"+\
            "<p><Strong>Autor:</strong>" +str(comentario.autor) +"</p>"+\
            "<p><Strong>Comentario:</strong>"+ str(comentario.texto) +"</p>"+\
            '<img class="camera_image" src=/'+comentario.imagen_camara+' alt="Imagen de la cámara">\
            </li>\
        </ul>'
    response = HttpResponse(html_content)
    return response

def comentario_dyn(request):
    manage_id_sesion(request)
    id = request.GET.get('id')
    camera = Camara.objects.get(id=id)
    if request.method == "POST":
        form = ComentarioForm(request.POST)
        if form.is_valid():
            # Esto hace que no se haga save automaticamente
            instancia = form.save(commit=False)
            # Aqui introduzco los datos comentario que quiero
            # que sean automaticos
            if False:
                instancia.imagen_camara.save(imagen.name,imagen,save=True)
            instancia.camara = camera
            instancia.autor = request.session['nombre_usuario']
            instancia.fecha = request.session['fecha']
            instancia.imagen_camara = download_image(camera.url)
            # Ahora si guardo el comentario
            instancia.save()
            return redirect('/see_camera_dyn/'+id)
    else:
        form = ComentarioForm()
        request.session['fecha'] = str(datetime.now())
        body = "body/aux_camera_dyn_form.html"
        data ={'form':form,
               'camera':camera,
               'fecha':datetime.now()}

    return render(request, body, data)
################### Parte de cargar los datos ###################
def load_from_xml_type1(name_list,pref_lis):
    # Verificar si el archivo XML existe en la ruta proporcionada
    xml_file_path="Te_Veo_App/Available_cam_lists/"+name_list+".xml"
    if not os.path.exists(xml_file_path):
        print(f"Error: El archivo XML no se encuentra en la ruta especificada: {xml_file_path}")
        return

    # Parsear el XML y guardar los datos en la base de datos
    tree = ET.parse(xml_file_path)
    root = tree.getroot()
    for camara_xml in root.findall('camara'):
        id_camara = pref_lis+camara_xml.find('id').text
        url = camara_xml.find('src').text
        info = camara_xml.find('lugar').text
        coordenadas = camara_xml.find('coordenadas').text
        latitude, longitude = coordenadas.split(',')

        lista_camaras, _ = ListaCamaras.objects.get_or_create(nombre=name_list)
        if not Camara.objects.filter(id=id_camara).exists():
            Camara.objects.create(id=id_camara, url=url, info=info, latitude=latitude, longitude=longitude, lista=lista_camaras)

    print("Datos del XML guardados exitosamente en la base de datos.")

def load_from_xml_type2(name_list, pref_lis):
    # Verificar si el archivo XML existe en la ruta proporcionada
    xml_file_path = f"Te_Veo_App/Available_cam_lists/{name_list}.xml"
    if not os.path.exists(xml_file_path):
        print(f"Error: El archivo XML no se encuentra en la ruta especificada: {xml_file_path}")
        return

    # Parsear el XML y guardar los datos en la base de datos
    tree = ET.parse(xml_file_path)
    root = tree.getroot()

    # Obtener o crear la lista de cámaras
    lista_camaras, _ = ListaCamaras.objects.get_or_create(nombre=name_list)

    for cam_xml in root.findall('cam'):
        id_camara = pref_lis + cam_xml.get('id')
        url = cam_xml.find('url').text
        info = cam_xml.find('info').text
        latitude = cam_xml.find('place/latitude').text
        longitude = cam_xml.find('place/longitude').text

        # Convertir latitud y longitud a float
        latitude = float(latitude)
        longitude = float(longitude)

        # Verificar si la cámara ya existe, si no, crearla
        if not Camara.objects.filter(id=id_camara).exists():
            Camara.objects.create(id=id_camara, url=url, info=info, latitude=latitude, longitude=longitude,
                                  lista=lista_camaras)

    print("Datos del XML guardados exitosamente en la base de datos.")


def load_from_xml_type3(name_list, pref_lis):
    # Verificar si el archivo XML existe en la ruta proporcionada
    xml_file_path = f"Te_Veo_App/Available_cam_lists/{name_list}.xml"
    if not os.path.exists(xml_file_path):
        print(f"Error: El archivo XML no se encuentra en la ruta especificada: {xml_file_path}")
        return

    # Parsear el XML y guardar los datos en la base de datos
    tree = ET.parse(xml_file_path)
    root = tree.getroot()

    # Navegar a la lista de cámaras
    cctv_camera_list = root.find('.//CctvCameraList')
    if cctv_camera_list is None:
        print("Error: No se encontró el nodo 'CctvCameraList' en el XML.")
        return

    for camara_xml in cctv_camera_list.findall('CctvCamera'):
        id_camara = pref_lis + camara_xml.get('id')
        url = camara_xml.find('.//Url').text
        info = camara_xml.find('Identification').text
        latitude = float(camara_xml.find('.//Latitude').text)
        longitude = float(camara_xml.find('.//Longitude').text)

        lista_camaras, _ = ListaCamaras.objects.get_or_create(nombre=name_list)
        if not Camara.objects.filter(id=id_camara).exists():
            Camara.objects.create(id=id_camara, url=url, info=info, latitude=latitude, longitude=longitude,
                                  lista=lista_camaras)

    print("Datos del XML guardados exitosamente en la base de datos.")
def load_list_data(request,name_list):
    if name_list == 'listado1':
        load_from_xml_type1(name_list,'LIS1-')
    elif name_list == 'listado2':
        load_from_xml_type2(name_list,'LIS2-')
    elif name_list == 'listado3':
        load_from_xml_type3(name_list,'LIS3-')
    return redirect('cameras_page')

def del_list_data(request,name_list):
    if ListaCamaras.objects.filter(nombre=name_list).exists():
        lista = ListaCamaras.objects.filter(nombre=name_list).first()
        lista.delete()
    return redirect('cameras_page')

################### A partir de aqui son funciones de decoracion, estilos e imagenes. ###################
def style_main(request):
    # Renderiza la plantilla CSS con los datos del contexto
    template = loader.get_template('Styles/main.css')
    css = template.render({'letter_size': request.session.get('letter_size'),
                           'letter_type': request.session.get('letter_type')})

    # Devuelve el CSS generado como una respuesta HTTP con el tipo de contenido adecuado
    response = HttpResponse(css, content_type='text/css')
    return response
def favicon(request):
    favicon_path = "./Te_Veo_App/images/favicon.ico"
    return FileResponse(open(favicon_path, 'rb'), content_type='image/x-icon')
def send_imagen(request,image_name):
    # Ruta completa de la imagen
    image_path = "Te_Veo_App/images/"+image_name
    # Verificar si el archivo existe
    if os.path.exists(image_path):
        file = open(image_path, 'rb')

        # Lee el contenido del archivo binario
        file_content = file.read()

        # Cierra el archivo
        file.close()

        # Crea una respuesta HTTP con el contenido de la imagen leído
        response = HttpResponse(file_content, content_type="image/jpeg")

        # Devuelve la respuesta HTTP
        return response
    else:
        # Si la imagen no existe, devolver un error 404
        return HttpResponse(status=404)

def download_image(url):
    path = 'Te_Veo_App/images/'+str(datetime.now().strftime('%Y%m%d%H%M%S'))+'.jpg'
    headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:100.0) Gecko/20100101 Firefox/100.0'
    }
    image = requests.get(url, headers=headers)
    with open(path, 'wb') as f:
        f.write(image.content)
        f.close()
    return path



