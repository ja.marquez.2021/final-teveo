from django.contrib import admin
from .models import Camara, ListaCamaras, Comentario, Session_db
from django.contrib.sessions.models import Session

# Aqui lo que hago es agregar al admin site los modelos
# para poder manipularlos y verlos
admin.site.register(Camara)
admin.site.register(ListaCamaras)
admin.site.register(Comentario)
admin.site.register(Session)
admin.site.register(Session_db)
