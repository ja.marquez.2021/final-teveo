from django.urls import path
from . import views

urlpatterns = [
    # Este recurso se una para cargar los datos de una lista.
    path('load_list_data/<name_list>', views.load_list_data, name='load_list_data'),
    # Este recurso se una para borrar los datos de una lista.
    path('del_list_data/<name_list>', views.del_list_data, name='del_list_data'),
    # Este recurso se usa para cambiar el estilo de la pagina y ponerte un nombre
    path('settings', views.settings, name='settings'),
    # Con este recurso se pide el css utilizado.
    path('main.css', views.style_main, name='style_main'),
    # Este recurso te lleva a la pagina con todas las camaras.
    path('cameras_page', views.cameras_page, name='cameras_page'),
    # Este recurso te lleva a la pagina de una camara concreta por su id.
    path('see_camera/<id>', views.camera_page, name='camera_page'),
    # Este recurso te lleva a la pagina dinamica de una camara concreta por su id.
    path('see_camera_dyn/<id>', views.dyn_page, name='dyn_page'),
    # Este recurso te lleva a la pagina del json de una camara concreta por su id.
    path('json_camara/<cam_id>', views.json_camara, name='json_camara'),
    # Este recurso se usa para dejar un comentario en una pagina.(info va en la query)
    path('comentario', views.comentar_camara, name='comentar_camara'),
    # Este recurso se una para que el servidor trate los formularios.
    path('formulario', views.camara_form, name='camara_form'),
    # Pagina de cerrar sesion
    path('close_sesion', views.close_sesion, name='close_sesion'),
    #(OBSOLETO) por algun motivo session ya no me deja acceder a el
    # Pagina de cerrar sesion (solo para admins)
    #path('close_sesions', views.close_sesions, name='close_sesions'),
    # Este recurso te lleva a la pagina de ayuda
    path('help', views.help, name='help'),
    # Este recurso se usa para pedir imagenes al servidor.
    path('Te_Veo_App/images/<image_name>', views.send_imagen, name='send_imagen'),
    # Este recurso se usa para pasar el campo html que representa una imagen pensado para la pagina dinamica
    path('imagen_dyn/<id>', views.imagen_dyn, name='image_dyn'),
    # Este recurso se usa para pasar los comentarios de la pagina dinamica
    path('comentarios_dyn/<id>', views.comentarios_dyn, name='comentarios_dyn'),
    # Este recurso se usa para el gestionar el formulario de la pagina dinamica
    path('comentario_dyn', views.comentario_dyn, name='comentario_dyn'),
    # favicon
    path('favicon.ico', views.favicon, name='favicon'),
    path('', views.main_page, name='main_page'),
]
