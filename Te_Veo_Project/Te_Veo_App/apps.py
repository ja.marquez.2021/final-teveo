from django.apps import AppConfig


class TeVeoAppConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "Te_Veo_App"
