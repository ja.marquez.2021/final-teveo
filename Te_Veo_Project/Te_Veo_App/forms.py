from django import forms
from .models import Camara
from .models import ListaCamaras
from .models import Comentario

class NameForm(forms.Form):
    your_name = forms.CharField(label="Your name", max_length=100)


class ComentarioForm(forms.ModelForm):
    class Meta:
        model = Comentario
        fields = ['texto']
