from django.test import TestCase
from django.urls import reverse
from .models import Camara,Comentario, Session_db,ListaCamaras

class MainPageTestCase(TestCase):
    def setUp(self):
        self.session = Session_db.objects.create(name="TestUser", letter_type="Arial", letter_size="12")

    def test_main_page_status_code(self):
        url = reverse('main_page')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_main_page_with_session_id(self):
        url = reverse('main_page') + f'?id={self.session.id}'
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)


class CamaraFormTestCase(TestCase):
    def test_camara_form_get(self):
        url = reverse('camara_form')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

class JsonCamaraTestCase(TestCase):
    def setUp(self):
        self.lista_camaras = ListaCamaras.objects.create(nombre="Lista 1")
        self.camera = Camara.objects.create(id="1", url="http://example.com", info="Camara 1", latitude=0.0, longitude=0.0, lista=self.lista_camaras)

    def test_json_camara_status_code(self):
        url = reverse('json_camara', args=[self.camera.id])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response['Content-Type'], 'application/json')

class HelpTestCase(TestCase):
    def test_help_page_status_code(self):
        url = reverse('help')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)


class CloseSesionTestCase(TestCase):
    def test_close_sesion(self):
        url = reverse('close_sesion')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 302)  # Redirección esperada

class Test_de_todo(TestCase):
    def setUp(self):
        self.session = Session_db.objects.create(name="TestUser", letter_type="Arial", letter_size="12")
    def test_mucho(self):
        nombre_prueba = "TestUser2"
        pagina_cameras = "/cameras_page"
        right_status_codes = [200, 200, 302, 200, 200, nombre_prueba, 302, pagina_cameras, 302, pagina_cameras]
        test_status = []
        # cargo pagina principal #200
        url = reverse('main_page')
        response = self.client.get(url)
        test_status.append(response.status_code)
        #voy a la pagina de camaras #200
        url = reverse('cameras_page')
        response = self.client.get(url)
        test_status.append(response.status_code)
        # cargo listado1 #302
        url = "/load_list_data/listado1"
        response = self.client.get(url)
        test_status.append(response.status_code)
        # Voy a la pagina de ayuda #200
        url = "/help"
        response = self.client.get(url)
        test_status.append(response.status_code)
        # voy a cambiar nombre el nombre con un POST #200
        url = "/settings"
        response = self.client.post(url,{"new_name":nombre_prueba})
        test_status.append(response.status_code)
        nombre_changes = str(response.context['metadata']["session_name"])
        test_status.append(nombre_changes)
        # probamos a borrar la lista de camaras 2 (para ver que no explota el programa) #302
        url = "/del_list_data/listado2"
        response = self.client.get(url)
        test_status.append(response.status_code)
        # pagina a la que soy redirigido #/cameras_page
        pagina_redirected = str(response.headers).split(" ")[4][1:-2]
        test_status.append(pagina_redirected)
        # probamos a borrar la lista de camaras 1 #302
        url = "/del_list_data/listado2"
        response = self.client.get(url)
        test_status.append(response.status_code)
        # pagina a la que soy redirigido #/cameras_page
        pagina_redirected = str(response.headers).split(" ")[4][1:-2]
        test_status.append(pagina_redirected)
        self.assertEqual(test_status, right_status_codes)






