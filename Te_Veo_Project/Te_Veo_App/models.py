from django.db import models

# IMPORTANTE!!!
# Usamos Pillow para guardar la imagen
# si no lo tienes instalalo por favor.


# Aqui creo una lista de camaras, su proposito
# es agrupar camaras por listas por si se quiere
# hacer referencia a un grupo de camaras.
class ListaCamaras(models.Model):
    nombre = models.CharField(max_length=100)

    def __str__(self):
        return self.nombre
# Esta clase contiene todos los datos de una camara.
class Camara(models.Model):
    id = models.CharField(max_length=100, primary_key=True)
    url = models.URLField()
    info = models.CharField(max_length=255)
    latitude = models.FloatField()
    longitude = models.FloatField()
    # Esto lo que indica es que la relacion que guarda la lita de camaras con las camaras.
    # Le estamos diciendo que se puede asociar una camara a un elemento de la clase Lista camaras
    # ademas que si se borra una Lista de camaras se borre tambien las camaras que contiene.
    lista = models.ForeignKey(ListaCamaras, on_delete=models.CASCADE, related_name='camaras')

    def __str__(self):
        return f"Camara {self.id}: {self.info}"

class Session_db(models.Model):
    id = models.CharField(max_length=32,primary_key=True)
    name = models.CharField(max_length=100,null=True)
    letter_type = models.CharField(max_length=100,null=True)
    letter_size = models.CharField(max_length=100,null=True)
    # not implemented
    theme_type = models.CharField(max_length=100,null=True)

# Esta clase tiene todos los datos de un comentario.
class Comentario(models.Model):
    # Aqui indico que cada comentario tendra una camara asociada, y si se borra la camara,
    # el comentario tambien.
    camara = models.ForeignKey(Camara, on_delete=models.CASCADE)
    # Por defecto la opcion esta a false, si se pone en true a la hora de crear este modelo se
    # pone la fecha en ese instante.
    fecha = models.DateTimeField(auto_now_add=False)
    autor = models.CharField(max_length=100,null=True)
    texto = models.TextField(null=True)
    imagen_camara = models.CharField(max_length=100,null=True)

    def __str__(self):
        return f"Comentario para la cámara {self.camara} - {self.fecha}"



